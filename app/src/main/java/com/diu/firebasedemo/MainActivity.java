package com.diu.firebasedemo;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.diu.firebasedemo.model.PostDM;
import com.diu.firebasedemo.utils.BitmapUtils;
import com.diu.firebasedemo.utils.PersistentUser;
import com.diu.firebasedemo.viewholder.PostViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private Context context;
    private EditText title,description;
    private DatabaseReference mReferance;
    private RecyclerView mRecyclerView;
    private FirebaseRecyclerAdapter<PostDM, PostViewHolder> mAdapter;
    private Uri outputFileUri;
    private int SELECT_POST_IMG=100,CREATE_POST_IMG=200;
    private Uri selectedImage;
    private String selectedPostImg = "",base64Image;
    public static final String APP_TEMP_FOLDER = "diu";
    private ImageView cameraImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context=this;
        mReferance= FirebaseDatabase.getInstance().getReference();
        initUi();
    }

    private void initUi() {
        title=(EditText)findViewById(R.id.title);
        cameraImage=(ImageView)findViewById(R.id.cameraImage);
        description=(EditText)findViewById(R.id.description);
        mRecyclerView=(RecyclerView)findViewById(R.id.messages_list);
        LinearLayoutManager mManager = new LinearLayoutManager(context);
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        mRecyclerView.setLayoutManager(mManager);
        cameraImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.chang_photo_dialogue);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

                LinearLayout tvUseCam = (LinearLayout) dialog.findViewById(R.id.tvUseCam);
                LinearLayout tvRoll = (LinearLayout) dialog.findViewById(R.id.tvRoll);
                LinearLayout tvCance = (LinearLayout) dialog.findViewById(R.id.tvCance);
                tvRoll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        imageFromGallery();
//                Toast.makeText(context,"Not implemented now",Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });


                tvUseCam.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        imageFromCamera();
//                Toast.makeText(context,"Not implemented now",Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });

                tvCance.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        // Set up FirebaseRecyclerAdapter with the Query

        Query postsQuery = mReferance.child("posts").limitToFirst(100);
        mAdapter = new FirebaseRecyclerAdapter<PostDM, PostViewHolder>(PostDM.class, R.layout.item_post,
                PostViewHolder.class, postsQuery) {
            @Override
            protected void populateViewHolder(final PostViewHolder viewHolder, final PostDM model, final int position) {
                final DatabaseReference postRef = getRef(position);

                // Set click listener for the whole post view
                final String postKey = postRef.getKey();
                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Launch PostDetailActivity
//                        Intent intent = new Intent(context, MainActivity.class);
//                        intent.putExtra("id", postKey);
//                        startActivity(intent);
                    }
                });

                // Determine if the current user has liked this post and set UI accordingly
                if (model.stars.containsKey(PersistentUser.getUserID(context))) {
                    viewHolder.starView.setImageResource(R.drawable.ic_toggle_star_24);
                } else {
                    viewHolder.starView.setImageResource(R.drawable.ic_toggle_star_outline_24);
                }

                // Bind Post to ViewHolder, setting OnClickListener for the star button
                viewHolder.bindToPost(model, new View.OnClickListener() {
                    @Override
                    public void onClick(View starView) {
                        // Need to write to both places the post is stored
                        DatabaseReference globalPostRef = mReferance.child("posts").child(postRef.getKey());
                        DatabaseReference userPostRef = mReferance.child("user-posts").child(model.uid).child(postRef.getKey());

                        // Run two transactions
                        onStarClicked(globalPostRef);
                        onStarClicked(userPostRef);
                    }
                });
            }
        };
        mRecyclerView.setAdapter(mAdapter);
    }

    // [START post_stars_transaction]
    private void onStarClicked(DatabaseReference postRef) {
        postRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                PostDM p = mutableData.getValue(PostDM.class);
                if (p == null) {
                    return Transaction.success(mutableData);
                }

                if (p.stars.containsKey(PersistentUser.getUserID(context))) {
                    // Unstar the post and remove self from stars
                    p.starCount = p.starCount - 1;
                    p.stars.remove(PersistentUser.getUserID(context));
                } else {
                    // Star the post and add self to stars
                    p.starCount = p.starCount + 1;
                    p.stars.put(PersistentUser.getUserID(context), true);
                }

                // Set value and report transaction success
                mutableData.setValue(p);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b,
                                   DataSnapshot dataSnapshot) {
                // Transaction completed
                Log.d("main", "postTransaction:onComplete:" + databaseError);
            }
        });
    }

    public void createPost(View view) {
        if (TextUtils.isEmpty(title.getText().toString())){
            Toast.makeText(context,"Please enter title",Toast.LENGTH_SHORT).show();

        }else if (TextUtils.isEmpty(description.getText().toString())){
            Toast.makeText(context,"Please enter Description",Toast.LENGTH_SHORT).show();
        }else{

            if (!TextUtils.isEmpty(selectedPostImg)){
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 8; // shrink it down otherwise we will use stupid amounts of memory
                Bitmap bitmap = BitmapFactory.decodeFile(selectedPostImg, options);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] bytes = baos.toByteArray();
                base64Image = Base64.encodeToString(bytes, Base64.DEFAULT);

            }

            String key = mReferance.child("posts").push().getKey();
            PostDM post = new PostDM();
            post.setUid(PersistentUser.getUserID(context));
            post.setAuthor(PersistentUser.getUserName(context));
            post.setTitle(title.getText().toString());
            post.setBody(description.getText().toString());
            post.setImageUrl(base64Image);
            Map<String, Object> postValues = post.toMap();

            Map<String, Object> childUpdates = new HashMap<>();
            childUpdates.put("/posts/" + key, postValues);
            childUpdates.put("/user-posts/" + post.getUid() + "/" + key, postValues);
            mReferance.updateChildren(childUpdates, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    if (databaseError==null){
                        title.setText("");
                        description.setText("");
                        Toast.makeText(context,"Post added successful",Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(context,"Post added Failed",Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==SELECT_POST_IMG){
            if (resultCode == Activity.RESULT_OK && data != null) {
                selectedImage = data.getData();
                selectedPostImg = getImageUrlWithAuthority(context, selectedImage, "post.jpg");

                try {

                    selectedPostImg = Environment.getExternalStorageDirectory() + File.separator + APP_TEMP_FOLDER + File.separator + "post.jpg";
                    File file = new File(selectedPostImg);
                    BitmapUtils.rotateAndSaveImg(context,file,selectedImage);
                    cameraImage.setImageURI(Uri.fromFile(file));

                } catch (Exception e) {

                    Log.e("OnSelectPostImage", e.getMessage());
                }
            }

        }else if (requestCode==CREATE_POST_IMG){

            try {

                selectedPostImg = Environment.getExternalStorageDirectory() + File.separator + APP_TEMP_FOLDER + File.separator + "post.jpg";
                File file = new File(selectedPostImg);
                BitmapUtils.rotateAndSaveImg(context,file,selectedImage);
                cameraImage.setImageURI(Uri.fromFile(new File(selectedPostImg)));

            } catch (Exception ex) {

                Log.v("OnCameraCallBack", ex.getMessage());
            }

        }
    }

    File file;
    private static File dir = null;
    public static String getImageUrlWithAuthority(Context context, Uri uri, String fileName) {

        InputStream is = null;

        if (uri.getAuthority() != null) {

            try {

                is = context.getContentResolver().openInputStream(uri);
                Bitmap bmp = BitmapFactory.decodeStream(is);

                return writeToTempImageAndGetPathUri(context, bmp, fileName).toString();

            } catch (FileNotFoundException e) {

                e.printStackTrace();

            } finally {

                try {

                    if (is != null) {

                        is.close();
                    }

                } catch (IOException e) {

                    e.printStackTrace();
                }
            }
        }

        return null;
    }

    public static String writeToTempImageAndGetPathUri(Context inContext, Bitmap inImage, String fileName) {

        String file_path = Environment.getExternalStorageDirectory() + File.separator + APP_TEMP_FOLDER;
        File dir = new File(file_path);
        if (!dir.exists()) dir.mkdirs();

        File file = new File(dir, fileName);

        try {

            FileOutputStream fos = new FileOutputStream(file);

            inImage.compress(Bitmap.CompressFormat.JPEG, 100, fos);

            fos.flush();
            fos.close();

        } catch (FileNotFoundException e) {

            Toast.makeText(inContext, "Error occured. Please try again later.", Toast.LENGTH_SHORT).show();

        } catch (IOException e) {

            e.printStackTrace();
        }

        return Environment.getExternalStorageDirectory() + File.separator + APP_TEMP_FOLDER + File.separator + "post.jpg";
    }

    public void imageFromGallery() {

        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(Intent.createChooser(intent, "Gallery"), SELECT_POST_IMG);
    }

    public void imageFromCamera() {

        try {

            File root = new File(Environment.getExternalStorageDirectory(), APP_TEMP_FOLDER);

            if (!root.exists()) {

                root.mkdirs();
            }

            File sdImageMainDirectory = new File(root, "post.jpg");
            outputFileUri = Uri.fromFile(sdImageMainDirectory);
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            startActivityForResult(cameraIntent, CREATE_POST_IMG);

        } catch (Exception e) {

            Toast.makeText(context, "Error occured. Please try again later.", Toast.LENGTH_SHORT).show();
        }
    }

}
