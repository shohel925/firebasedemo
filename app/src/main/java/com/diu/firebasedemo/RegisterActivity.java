package com.diu.firebasedemo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.diu.firebasedemo.model.User;
import com.diu.firebasedemo.utils.PersistentUser;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class RegisterActivity extends AppCompatActivity {
    private Context context;
    private EditText register_email,register_password,fName,lName;
    private Button register_Submit;
    private FirebaseAuth mAuth;
//    private FirebaseAuth.AuthStateListener mAuthListener;
    private String TAG="RegisterActivity.class";
    private DatabaseReference mDatabase;
    private ProgressBar registerProgress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        context=this;
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        initUi();
    }
    private void initUi() {
        registerProgress=(ProgressBar)findViewById(R.id.registerProgress);
        fName=(EditText)findViewById(R.id.fName);
        lName=(EditText)findViewById(R.id.lName);
        register_email=(EditText)findViewById(R.id.register_email);
        register_password=(EditText)findViewById(R.id.register_password);
        register_Submit=(Button)findViewById(R.id.register_Submit);
        register_Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(fName.getText().toString())){
                    Toast.makeText(context,"Please Enter your First Name",Toast.LENGTH_SHORT).show();
                }else if(TextUtils.isEmpty(lName.getText().toString())){
                    Toast.makeText(context,"Please Enter your Last Name",Toast.LENGTH_SHORT).show();
                }else if(TextUtils.isEmpty(register_password.getText().toString())){
                    Toast.makeText(context,"Please Enter your Email",Toast.LENGTH_SHORT).show();
                }else if(TextUtils.isEmpty(register_password.getText().toString())){
                    Toast.makeText(context,"Please Enter your Password",Toast.LENGTH_SHORT).show();
                }else{
                    registerProgress.setVisibility(View.VISIBLE);
                    mAuth.createUserWithEmailAndPassword(register_email.getText().toString(), register_password.getText().toString())
                            .addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    Log.d("login", "createUserWithEmail:onComplete:" + task.isSuccessful());

                                    // If sign in fails, display a message to the user. If sign in succeeds
                                    // the auth state listener will be notified and logic to handle the
                                    // signed in user can be handled in the listener.
                                    if (!task.isSuccessful()) {
                                        Toast.makeText(context, "This Email already used..", Toast.LENGTH_SHORT).show();
                                    }else{
                                        FirebaseUser user=task.getResult().getUser();
                                        String username = usernameFromEmail(user.getEmail());
                                        writeNewUser(user.getUid(),username,user.getEmail());
                                    }
                                }
                            });


                }
            }
        });
    }

    private String usernameFromEmail(String email) {
        if (email.contains("@")) {
            return email.split("@")[0];
        } else {
            return email;
        }
    }

    // [START basic_write]
    private void writeNewUser(final String userId, String name, String email) {
        User user = new User();
        user.setfName(fName.getText().toString());
        user.setlName(lName.getText().toString());
        user.setEmail(email);
        user.setUsername(name);
        mDatabase.child("users").child(userId).setValue(user, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError==null){
                    mDatabase.child("users").child(userId).addListenerForSingleValueEvent(
                            new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    // Get user value
                                    User user = dataSnapshot.getValue(User.class);
                                    registerProgress.setVisibility(View.GONE);
                                    // [START_EXCLUDE]
                                    if (user == null) {
                                        // User is null, error out
                                        Toast.makeText(RegisterActivity.this, "Error: could not fetch user.",
                                                Toast.LENGTH_SHORT).show();
                                    } else {
                                        // Write new post
                                        Toast.makeText(context, "SignUp Successful..", Toast.LENGTH_SHORT).show();
                                        Log.e("Email","user"+user.getEmail());
                                        PersistentUser.setLogin(context);
                                        PersistentUser.setUserEmail(context,user.getEmail());
                                        PersistentUser.setUserID(context,userId);
                                        PersistentUser.setUserName(context,user.getUsername());
                                        startActivity(new Intent(context,MainActivity.class));
                                        finish();
                                    }

                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.w(TAG, "getUser:onCancelled", databaseError.toException());
                                }
                            });

                }

            }
        });

    }
    // [END basic_write]
    public void LogOut(View view) {
        mAuth.signOut();
    }

}
