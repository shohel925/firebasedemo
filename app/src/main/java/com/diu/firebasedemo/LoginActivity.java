package com.diu.firebasedemo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.diu.firebasedemo.model.User;
import com.diu.firebasedemo.utils.PersistentUser;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LoginActivity extends AppCompatActivity {
    private Context context;
    private EditText email,password;
    private FirebaseAuth mAuth;
    private String TAG ="Login Activity";
    private ProgressBar loginProgress;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        context=this;
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        initUi();

    }

    private void initUi() {
        email=(EditText)findViewById(R.id.email);
        password=(EditText)findViewById(R.id.password);
        loginProgress=(ProgressBar)findViewById(R.id.loginProgress);
        
    }

    public void loginBtn(View view) {
        if (TextUtils.isEmpty(email.getText().toString())){
            Toast.makeText(context,"Please Enter your Email",Toast.LENGTH_SHORT).show();

        }else if(TextUtils.isEmpty(password.getText().toString())){
            Toast.makeText(context,"Please Enter your Password",Toast.LENGTH_SHORT).show();
        }else{
            loginProgress.setVisibility(View.VISIBLE);
            mAuth.signInWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(final Task<AuthResult> task) {
                            // If sign in fails, display a message to the user. If sign in succeeds
                            // the auth state listener will be notified and logic to handle the
                            // signed in user can be handled in the listener.
                            if (!task.isSuccessful()) {
                                loginProgress.setVisibility(View.GONE);
                                Toast.makeText(LoginActivity.this, "Email and password not match", Toast.LENGTH_SHORT).show();
                            }else{
                                mDatabase.child("users").child(task.getResult().getUser().getUid()).addListenerForSingleValueEvent(
                                        new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                // Get user value
                                                User user = dataSnapshot.getValue(User.class);
                                                loginProgress.setVisibility(View.GONE);
                                                // [START_EXCLUDE]
                                                if (user == null) {
                                                    // User is null, error out
                                                    Toast.makeText(LoginActivity.this, "Error: could not fetch user.",
                                                            Toast.LENGTH_SHORT).show();
                                                } else {
                                                    // Write new post
                                                    Toast.makeText(context, "SignUp Successful..", Toast.LENGTH_SHORT).show();
                                                    Log.e("Email","user"+user.getEmail());
                                                    PersistentUser.setLogin(context);
                                                    PersistentUser.setUserEmail(context,user.getEmail());
                                                    PersistentUser.setUserID(context,task.getResult().getUser().getUid());
                                                    PersistentUser.setUserName(context,user.getUsername());
                                                    startActivity(new Intent(context,MainActivity.class));
                                                    finish();
                                                }

                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {
                                                Log.w(TAG, "getUser:onCancelled", databaseError.toException());
                                            }
                                        });
                            }
                        }
                    });


        }
    }

    public void logOut(View view) {
        mAuth.signOut();
    }

    public void registerBtn(View view) {
        startActivity(new Intent(context,RegisterActivity.class));
    }
}
