package com.diu.firebasedemo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.diu.firebasedemo.utils.PersistentUser;

public class SplashActivity extends AppCompatActivity {
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        context=this;
        initUi();
    }

    private void initUi() {

        Handler handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
               if (PersistentUser.isLogged(context)){
                   startActivity(new Intent(context,MainActivity.class));
               }else{
                   startActivity(new Intent(context,LoginActivity.class));
               }
                finish();
            }
        },2000);

    }
}
